#!/bin/bash

# Print the front matter
cat <<!
---
title: "A curated list of yoga poses"
date: 2019-10-08T22:08:03+01:00
tags:
  - yoga
  - sanskrit
draft: false
---

!

# Print the table header
echo "| $(head -1 asana.csv  | tr ',' '|') |"
echo "|---|---|---|---|---|---|"

# Print the asana list
tail +2 asana.csv | head -10 | while read line; do echo "| $(echo $line | tr ',' '|') |"; done
